<?php
/**
 * @package EMSI-API
 */
/*
Plugin Name: EMSI API
Description: A plugin to open connection with EMSI API and to house static methods for template usage in the theme layer.
Version: 0.0.1
Author: Blue Anvil Mkt
License: GPLv2 or later
Text Domain: emsi-api
*/

if ( ! defined( 'WPINC' ) ) die;

define( 'EMSI_API_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

// ENQUEUE SCRIPTS
require_once( EMSI_API_PLUGIN_DIR . 'includes/enqueue-scripts.php' );

// CLASSES
require_once( EMSI_API_PLUGIN_DIR . 'classes/class.emsi-connection.php' );

// AJAX ACTIONS
require_once( EMSI_API_PLUGIN_DIR . 'includes/emsi-ajax-connection.php' );

// SHORTCODES TO DISPLAY IN TEMPLATE
require_once( EMSI_API_PLUGIN_DIR . 'includes/emsi-shortcodes.php' );