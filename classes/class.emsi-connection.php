<?php
require_once(EMSI_API_PLUGIN_DIR . 'config.php');

class EMSI_CONNECTION {
    const API_KEY = EMSI_API_KEY;
    const API_URL = "https://api.emsicc.com";

    public static function get_api_key() {
        return self::API_KEY;
    }

    public static function get_url_endpoint( $endpoint, $params ) {
        $endpoint = $params ? $endpoint . $params : $endpoint;

        $api_url = self::API_URL . $endpoint;

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $api_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "X-Api-Key: " . self::API_KEY
            ],
        ]);

        $response = curl_exec($curl);

        // For timing ** COMMENT OUT FOR PROD **
        // $info = curl_getinfo($curl);
        // echo 'Took ' . $info['total_time'] . ' seconds to transfer.';

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        }

        return $response;
    }

}