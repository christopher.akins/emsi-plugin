# EMSI PLUGIN

This plugin pulls data from the EMSI API in order to display on the front-end. This plugin, currently, does not store any data in the DB, so there is no need to "clean up" once the plugin is deactivated and removed. This is more/less a front-end plugin that makes one server side request prior to page load, which will retrieve all the careers related to a specific Program (Software Dev, Data Science, etc.).

## Build Process

To install node packages, run `docker-compose run --rm npm install`.

Then run `docker-compose run --rm npm run dev` to run the other node scripts. For instance `npm run prod` for production.

This will essentially run the scripts within the package.json file based off the Webpack settings. The source files are within the `/src` directory. Compiled files should be created within the `/dist` folder.

`/src/js/main.js` will be the entry point for the application.

## Reef and Chartist... oh, and jQuery

Two important 3rd party libraries are imported: `Reef.js` and `Chartist.js`. Reef is basically a very, very lightweight state-based library and not as robust as React. It's simply used to populate the data within the table, once it's fetched from the API. Chartist is then, of course, used for charts and graphs to display some of the key data points for each career.

jQuery is used for easier AJAX calls, as well as some simple UI transitions, and since it's already loaded in WP, it's being used here for ease.

## The Shortcode

The shortcode(s) can be found in the `/includes/emsi-shortcodes.php`. This is where future shortcodes can be specified and added throughout the theme in any layout/template once the plugin is active.

## Configuration

Ensure that a `config.php` file is not included in the repo, but that it is added to all dev environments. The API key shouldn't really impact anything, as it's more of a read only key, but it's probably a good idea to keep this information hidden. Reach out to EMSI or Dayton Hansen for the key. Here are examples of what needs to be, or can be, included in this config file:

<pre>
define('EMSI_API_KEY', '{YOUR API KEY}');

/// SOME DEFAULT VALUES @TODO:// Change to Plugin Settings in Admin

define('EMSI_API_PROGRAM_DEFAULT', 'software-developer-program');
define('EMSI_API_REGION_DEFAULT', 'nation');
define('EMSI_API_GEO_ID_DEFAULT', '0'); // 0 for US
</pre>

## Updates
