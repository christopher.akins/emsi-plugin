/* eslint-disable no-mixed-spaces-and-tabs */
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
// const IgnoreEmitPlugin = require("ignore-emit-webpack-plugin");

module.exports = {
  entry: { main: './index.js' },
  output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name].js'
  },
  module: {
	rules: [
  	{
    	test: /\.js$/,
    	exclude: /node_modules/,
    	use: {
     		loader: "babel-loader",
				options: {
					presets: ['@babel/preset-env']
				}
    	}
  	},
  	{
    	test: /(\.css$|\.scss$)/,
    	use:  [
        	{ loader: 'style-loader' },
        	{ loader: MiniCssExtractPlugin.loader, options: { esModule: false } },
        	{ loader: 'css-loader', options: { sourceMap: true } },
        	{ loader: 'postcss-loader' },
        	{ loader: 'sass-loader', options: { sourceMap: true } }
    	],
  	}
	]
  },
  plugins: [
    new MiniCssExtractPlugin({
      chunkFilename: "[id].css",
        filename: 'style.css'
    })
  ]
};
