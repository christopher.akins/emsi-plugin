import Reef from 'reefjs';

import { iterateSkills } from './template-functions/soft-skills';

// import { annualEarningsChartOptions } from './template-functions/annual-earnings-chart';
// import { educationChartOptions } from './template-functions/education';

let app = new Reef('#emsi_data', {
  data: {
    careers: [],
    jobs: []
  },
  template: mainTemplate
});

function mainTemplate(props, elem) {

  // If there are careers, remove the overaly, then return the template data
  if (!props.careers.length) {
    document.querySelector('.loading-content').style.display = "block";
  } else {
    document.querySelector('.loading-content').style.display = "none";
  }

  return `
    <div class="careers-list-table">
      ${props.careers.map((career, index) => {
        // var employmentPastRate = career['employment'].map(calculatePastGrowthRate).join('');
        // var employmentFutureRate = career['employment'].map(calculateFutureGrowthRate).join('');
        // edChartData.push(career['education-attainment-levels']);
        annualEarningsChartData.push(career['annual-earnings']);
        jobGrowthProjectionsChartData.push(career['employment']);
        
        return `
          <div class="tr career-row" data-career-row="${index}">
            <div class="tc tc-body tc-occupation"><span class="plus-minus"></span> ${career['humanized-title']}</div>
            <div class="tc tc-body tc-earnings"><span>\$${Math.floor(career['annual-earnings'][0]['earnings']).toLocaleString()}</span></div>
            <div class="tc tc-body tc-openings">
              ${career['annual-openings'].toLocaleString()}
            </div>
          </div>
          <div class="career-dropdown-container" data-career-dropdown="${index}">
            <div class="collapse-tab">Collapse</div>

            <div class="annual-earnings-container">
                <h5>Wages</h5>
                <p>New workers generally start around $${Math.floor(career['annual-earnings'][0]['earnings']).toLocaleString()}. Normal pay for ${career['humanized-title']} is $${Math.floor(career['annual-earnings'][2]['earnings']).toLocaleString()} per year, while highly experienced workers can earn as much as  $${Math.floor(career['annual-earnings'][4]['earnings']).toLocaleString()}.</p>
                <div class="annual-earnings-chart chart-container">
                  <div class="ct-chart ct-major-eleventh" id="ae_chart_${index}"></div>
                </div>
                <div style="text-align:center;margin-top:-25px;margin-bottom:20px;"><p>PERCENTILE</p></div>
            </div>

            <div class="job-growth-projections-container">
              <h5>Employment Projections</h5>
              <p>There are currently around ${career['employment'][2]['number'].toLocaleString()} ${career['humanized-title']} employed in the United States.</p>
              <div class="job-growth-chart chart-container">
                <div class="ct-chart ct-major-eleventh" id="jg_chart_${index}"></div>
              </div>
            </div>

            <div class="skills-list-container">
              <h5>Soft Skills</h5>
              ${career['skills'].map(iterateSkills).join('')}
            </div>

          </div>
        `;
      }).join('')}
    </div> <!-- // .careers-list-table -->
  `;

  // TOP ROW STATS EXAMPLE --

  // <div class="top-row-stats">
  //   <div class="stat">
  //     <img class="stat-icon" src="/wp-content/plugins/emsi-api/assets/img/annual-wage-emsi.png" alt="chart-graphic" />
  //     <p class="title">Avg. Starting Salary</p>
  //     <p class="data">\$${Math.floor(career['annual-earnings'][0]['earnings']).toLocaleString()}</p>
  //   </div>
  //   <div class="stat">
  //     <img class="stat-icon" src="/wp-content/plugins/emsi-api/assets/img/job-growth-emsi.png" alt="chart-graphic" />
  //     <p class="title">Annual Openings</p>
  //     <p class="data">${career['annual-openings'].toLocaleString()}</p>
  //   </div>
  // </div>


  // Template literal for the Education Attainment Levels Chart

  //<div class="btn-select-modal" data-career-btn="${index}">SELECT</div>
  // <div class="career-modal-overlay" data-career-modal="${index}">
  //   <div class="career-modal-container">
  //     <div class="btn-close">CLOSE</div>
  //     ${careerJob(career)}
  //   </div>
  // </div>

  // <div class="stat">
  //   <div class="stat-icon-wrapper"></div>
  //   <p class="title">Education Level</p>
  //   <p class="data">${career['typical-ed-level']}</p>
  // </div>
}

app.render();

export { app as default };