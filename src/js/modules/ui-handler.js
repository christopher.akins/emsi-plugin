(function(window, document, $) {

  // We have to add an event listener to determine if ReefJS has rendered the elements to the page
  // Then we apply the jQuery event listeners - using jQuery for ease
  document.addEventListener('reef:render', function(event) {

    // For the accordion within the table
    $('.tc-occupation').on('click', function(e) {

      // get the parent row container
      var parentRow = $(this).parent('.career-row');
      
      // Toggle classes
      $(this).toggleClass('dropdown-active');
      parentRow.toggleClass('dropdown-active');

      var rowId = parentRow.data('career-row');

      var dropdownEl = "[data-career-dropdown=" + rowId +"]";
      // a split second to hide the chart for the height
      // Find the chart container, all this to avoid FOUC with the chart rendering its height/width

      var chartContainer = $(dropdownEl).find('.chart-container');
      chartContainer.css('visibility', 'hidden');

      //$(dropdownEl).toggleClass('dropdown-active');
      $(dropdownEl).slideToggle(400, function(e) {
        window.dispatchEvent(new Event('resize'));
        chartContainer.css('visibility', 'visible');
      }).toggleClass('dropdown-active');

    });

    $('.collapse-tab').on('click', function(e) {
      var theParent = $(this).parent('.career-dropdown-container');
      var rowId = theParent.data('career-dropdown');

      var careerRow = "[data-career-row=" + rowId + "]";
      $(careerRow).toggleClass('dropdown-active');
      $(careerRow).children('.tc-occupation').toggleClass('dropdown-active');

      theParent.slideToggle().toggleClass('dropdown-active');
    });

    // For the modal - possible future use case

    // $('.btn-select-modal').on('click', function(e) {
    //   // first remove all active buttons/modals
    //   $('.modal-active').removeClass('modal-active');

    //   // Then start to add the classes back for the specific career row
    //   var parentRow = this.closest('.career-row');

    //   $(this).toggleClass('modal-active');
    //   $(parentRow).toggleClass('modal-active');

    //   var rowId = $(parentRow).data('career-row');

    //   var modalOverlayEl = "[data-career-modal=" + rowId +"]";

    //   $(modalOverlayEl).toggleClass('modal-active');
    // });

    // $('.btn-close').on('click', function(e) {
    //   $('.modal-active').removeClass('modal-active');
    // });

  });

})(window, document, jQuery);