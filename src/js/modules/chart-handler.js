
(function(window, document) {

  // We have to add an event listener to determine if ReefJS has rendered the elements to the page
  // Then we apply the jQuery event listeners - using jQuery for ease and understanding of code
  // Using Reefjs as a simple/low-weight front-end framework
  
  document.addEventListener('reef:render', function(event) {

    // Custom code for plugin to display labels near data points. 

    var label_defaultOptions = {
      labelClass: 'ct-label',
      labelOffset: {
        x: 0,
        y: -10
      },
      textAnchor: 'middle',
      align: 'center',
      labelInterpolationFnc: Chartist.noop
    };

    var label_labelPositionCalculation = {
      point: function(data) {
        return {
          x: data.x,
          y: data.y
        };
      },
      bar: {
        left: function(data) {
          return {
            x: data.x1,
            y: data.y1
          };
        },
        center: function(data) {
          return {
            x: data.x1 + (data.x2 - data.x1) / 2,
            y: data.y1
          };
        },
        right: function(data) {
          return {
            x: data.x2,
            y: data.y1
          };
        }
      }
    };

    Chartist.plugins = Chartist.plugins || {};
    Chartist.plugins.ctPointLabels = function(options) {

      options = Chartist.extend({}, label_defaultOptions, options);

      function addLabel(position, data) {
        // if x and y exist concat them otherwise output only the existing value
        var value = data.value.x !== undefined && data.value.y ?
          (data.value.x + ', ' + data.value.y) :
          data.value.y || data.value.x;

        data.group.elem('text', {
          x: position.x + options.labelOffset.x,
          y: position.y + options.labelOffset.y,
          style: 'text-anchor: ' + options.textAnchor
        }, options.labelClass).text(options.labelInterpolationFnc(value));
      }

      return function ctPointLabels(chart) {
        if (chart instanceof Chartist.Line || chart instanceof Chartist.Bar) {
          chart.on('draw', function(data) {
            var positonCalculator = label_labelPositionCalculation[data.type] && label_labelPositionCalculation[data.type][options.align] || label_labelPositionCalculation[data.type];
            if (positonCalculator) {
              addLabel(positonCalculator(data), data);
            }
          });
        }
      };
    };

    if (window.annualEarningsChartData) {
      window.annualEarningsChartData.forEach((chart, index) => {
        
        let seriesData = chart.map(el => el['earnings']);

        var data = {
          labels: [
            '10th',
            '25th',
            '50th',
            '75th',
            '90th'
          ],
          series: [
            seriesData
          ]
        };

        var options = {
          low: 0,
          fullWidth: true,
          chartPadding: {
              top: 25,
              right: 25,
              bottom: 5,
              left: 0
          },
          axisX: {
            labelOffset: {
                x: -13,
                y: 0
            }
          },
          axisY: {
              showLabel: false,
              showGrid: false
          },
          plugins: [
            Chartist.plugins.ctPointLabels({
              labelClass: 'ct-point-label',
              textAnchor: 'middle',
              labelOffset: {
                x: -10,
                y: -10
              },
              labelInterpolationFnc: function(value) {return '$' + Math.round(value/1000) + 'K'}
            })
          ]
        };

        new Chartist.Line('#ae_chart_' + index, data, options);
      });

    }

    if (window.jobGrowthProjectionsChartData) {
      window.jobGrowthProjectionsChartData.forEach((chart, index) => {
        let seriesData = chart.map(el => el['number']);
        let years = chart.map(el => el['year']);

        var data = {
          labels: years,
          series: [
            seriesData
          ]
        };

        var options = {
          fullWidth: true,
          chartPadding: {
              top: 25,
              right: 25,
              bottom: 5,
              left: 25
          },
          axisX: {
            labelOffset: {
                x: -13,
                y: 0
            }
          },
          axisY: {
              showLabel: false,
              showGrid: false
          },
          plugins: [
            Chartist.plugins.ctPointLabels({
              labelClass: 'ct-point-label-jobs',
              textAnchor: 'middle',
              labelOffset: {
                x: -10,
                y: -10
              },
              labelInterpolationFnc: function(value) {return Math.round(value/1000).toLocaleString() + 'K'}
            })
          ]
        };

        new Chartist.Line('#jg_chart_' + index, data, options);
      });
    }

  });

})(window, document);