// IMPORTANT ** This file triggers the table to load once the section is in the Viewport

import emsiObj from '../emsi-fetch-data';

function onVisibilityChange(el, callback) {
  var old_visible = false;

  return function () {
    let visible = isElementInViewport(el);

    if (visible != old_visible) {
      old_visible = visible;
      if (typeof callback == 'function') {
        callback();
      }
    }
  }
  
}

function isElementInViewport(el) {
  const rect = el.getBoundingClientRect();

  return (
    rect.top >= 10 &&
    rect.left >= 0 &&
    rect.bottom <= (window.innerHeight + el.clientHeight || document.documentElement.clientHeight + el.clientHeight) && 
    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
}

const handler = onVisibilityChange(document.querySelector('.emsi-table'), function() {

  // Now we'll call the function that fetches all the Career Data from the array
  emsiObj.updateTemplateCareersData(careersArr);

  // Now remove the listener, since the above has been called/fetched
  removeEventListener('DOMContentLoaded', handler, false);
  removeEventListener('load', handler, false);
  removeEventListener('scroll', handler, false);
  removeEventListener('resize', handler, false);
});

addEventListener('DOMContentLoaded', handler, false);
addEventListener('load', handler, false);
addEventListener('scroll', handler, false);
addEventListener('resize', handler, false);