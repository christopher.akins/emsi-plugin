
/*
*
* CREATE A GLOBAL OBJECT
*
*/

let emsiObj = {

  getAllProgramCareers: function(programName) {
    let data = {
      action: 'pull_emsi_data',
      nonce_security: emsi_api_obj.nonce_security,
      program_name: programName,
      career_id: careerId || null
    }

    $.ajax({
      url: emsi_api_obj.ajaxurl,
      type: "POST",
      data: data,
      success: function(response) {
        responseHandler(response);
      }
    });

  },

  getAllCareerData: function(careersArr, program, region, geoId) {
    // var careerArr = JSON.parse(careerArr);
    // console.log(careersArr);
    
    const allCareers = [];

    function myAjaxPromise(options) {
      return new Promise(function (resolve, reject) {
        jQuery.ajax(options).done(resolve).fail(reject);
      });
    }

    let promiseCareerArr = careersArr.map(element => {
      let data = {
        action: 'pull_emsi_all_career_data',
        nonce_security: emsi_api_obj.nonce_security,
        career_id: element,
        program_name: program,
        region: region,
        geo_id: geoId
      }

      return myAjaxPromise({
        url: emsi_api_obj.ajaxurl,
        type: "POST",
        data: data
      }).then(
        function(data) {
          allCareers.push(data);
          return data;
        }
      ).catch(function errorHandler(error) {
        console.log(error);
      });

    });

    return promiseCareerArr;

    // INCLUDE THE FOLLOWING WHEN/WHERE MARKUP IS DISPLAYED ON FRONT-END

    // Promise.all(promiseCareerArr).then(function(results) {
    //   return results;
    // }).catch(function(err) {  
    //   console.log(err, 'This is in the promise all catch statement');
    // });

  }

};

export default emsiObj;
