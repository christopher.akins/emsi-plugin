
/*
*
* GLOBAL EMSI OBJ/CLASS FOR FUNCTIONS TO PULL EMSI DATA
*
*/

let emsiObj = {

  getAllProgramCareers: function(programName) {
    let data = {
      action: 'pull_emsi_data',
      nonce_security: emsi_api_obj.nonce_security,
      program_name: programName,
      career_id: careerId || null
    }

    $.ajax({
      url: emsi_api_obj.ajaxurl,
      type: "POST",
      data: data,
      success: function(response) {
        responseHandler(response);
      }
    });

  },

  getAllCareerData: function(careersArr) {
    
    const allCareers = [];

    let promiseCareerArr = careersArr.map(element => {
      const formData = new FormData();
      let program = window.shortcodeProgram || 'software-developer-program';
      let region = window.shortcodeRegion || 'nation';
      let geoId = window.shortcodeGeoId || '0';

      formData.append( 'action', 'pull_emsi_all_career_data' );
      formData.append( 'nonce_security', emsi_api_obj.nonce_security );
      formData.append( 'career_id', element );
      formData.append( 'program_name', program );
      formData.append( 'region', region );
      formData.append( 'geo_id', geoId );
      
      // Now push the fetch results to the array allCareers
      allCareers.push(fetch(emsi_api_obj.ajaxurl, {
        method: "POST",
        credentials: 'same-origin',
        body: formData
      })
      .then((response) => response.json())
      .then(function(data) {
        return data;
      }).catch(function errorHandler(error) {
        console.log(error);
      }));

    });

    
    return allCareers;

    // INCLUDE THE FOLLOWING WHEN/WHERE MARKUP IS DISPLAYED ON FRONT-END

    // Promise.all(promiseCareerArr).then(function(results) {
    //   console.log(results);
    //   return results;
    // }).catch(function(err) {  
    //   console.log(err, 'This is in the promise all catch statement');
    // });

  },

  getJobData: function(careersArr) {
    
    const allJobs = [];

    let promiseCareerArr = careersArr.map(element => {
      const formData = new FormData();
      let program = window.shortcodeProgram || 'software-developer-program';
      let region = window.shortcodeRegion || 'nation';
      let geoId = window.shortcodeGeoId || '0';

      formData.append( 'action', 'pull_emsi_job_data' );
      formData.append( 'nonce_security', emsi_api_obj.nonce_security );
      formData.append( 'career_id', element );
      formData.append( 'program_name', program );
      formData.append( 'region', region );
      formData.append( 'geo_id', geoId );
      
      // Now push the fetch results to the array allCareers
      allJobs.push(fetch(emsi_api_obj.ajaxurl, {
        method: "POST",
        credentials: 'same-origin',
        body: formData
      })
      .then((response) => response.json())
      .then(function(data) {
        return data;
      }).catch(function errorHandler(error) {
        console.log(error);
      }));

    });

    
    return allJobs;
  },

  updateTemplateCareersData: function(careersArr) {

    let emsiResultsPromise = Promise.all(
      emsiObj.getAllCareerData(careersArr)
    ).then(function(results) {
        // Log results for now
        console.log(results);
        // When all the results are provided, update the Main Template's data
        mainEmsiTemplate.data.careers = results;
        return results;
    }).catch(function(err) {  
        console.log(err, 'This is in the promise all catch statement');
    });

    return emsiResultsPromise;
  },

  updateTemplateJobsData: function(careersArr) {
    let emsiResultsPromise = Promise.all(
      emsiObj.getJobData(careersArr)
    ).then(function(results) {
        // Log results for now
        console.log(results);
        // When all the results are provided, update the Main Template's data
        mainEmsiTemplate.data.jobs = results;
        return results;
    }).catch(function(err) {  
        console.log(err, 'This is in the promise all catch statement');
    });

    return emsiResultsPromise;    
  }

};

export default emsiObj;