
import mainEmsiTemplate from './emsi-main-template'; // The main markup and template of the table, empty upon page load

import './modules/viewport-handler'; // triggers when the table is in user's viewport, updating the template
import './modules/ui-handler'; // Once "Reef" data is rendered, add these UI interaction even handlers for the table
import './modules/chart-handler'; // Once "Reef" data is rendered, add the charts within the dropdowns

// assign the main template to the window global as well as some arrays used for fetching data to use in <script> elements
window.mainEmsiTemplate = mainEmsiTemplate;
window.annualEarningsChartData = [];
window.jobGrowthProjectionsChartData = [];