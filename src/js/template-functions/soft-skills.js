
// This function outputs the skills list, only show top 10 skills. Change the constant if necessary

const skillsToShow = 5;

const iterateSkills = function(skill, index) {

  if (index < skillsToShow) {

    let skillImportancePercentage = (skill['importance']/5) * 100;

    return `
      <div class="skill-wrapper">
        <div class="skill-row-top">
          <div class="skill-name-wrapper">${skill['name']}</div>
          <div class="skill-importance-wrapper">
            <div class="importance-bar" style="width:${skillImportancePercentage}%"></div>
          </div>
        </div>
        <div class="skill-row-bottom">
          <div class="skill-description">${skill['description']}</div>
        </div>
      </div>

    `;
  }
}

export { iterateSkills }