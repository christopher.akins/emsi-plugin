
const roundPercentage = function(value, precision) {
  var multiplier = Math.pow(10, precision || 0);
  return Math.round(value * multiplier) / multiplier;
}

const calculatePastGrowthRate = function(employment, index, arr) {
  const currentYear = new Date().getFullYear();
  const firstYear = arr[0]['year'];
  const firstYearTotal = arr[0]['number'];
  const totalYears = currentYear - firstYear;
  
  if (employment['year'] == currentYear ) {

    let growthRate = arr[index]['number'] - firstYearTotal;
    growthRate = growthRate/firstYearTotal;
    growthRate = growthRate * 100;
    growthRate = growthRate/totalYears;
    let growthRateRounded = roundPercentage(growthRate, 2);

    let isPositive = growthRateRounded > 0 ? 'positive' : 'negative';

    return `
      <div class="growth-rate ${isPositive}">${growthRateRounded}%</div>
    `;
  }
}

const calculateFutureGrowthRate = function(emp, index, arr) {
  const currentYear = new Date().getFullYear();
  const lastYearInArr = arr[arr.length - 1]['year'];
  const lastYearInArrTotal = arr[arr.length - 1]['number'];
  const totalYears = lastYearInArr - currentYear;

  if (emp['year'] == currentYear) {
    let growthRate = lastYearInArrTotal - arr[index]['number'];
    growthRate = growthRate/arr[index]['number'];
    growthRate = growthRate * 100;
    growthRate = growthRate/totalYears;

    let growthRateRounded = roundPercentage(growthRate, 2);
    let isPositive = growthRateRounded > 0 ? 'positive' : 'negative';

    return `
      <div class="growth-rate ${isPositive}">${growthRateRounded}%</div>
    `;
  }

}


const careerJob = function(career) {
  return `
    <h4>${career['humanized-title']}</h5>
    <div class="employment-data-row">
      <div class="annual-openings-block">
        <div class="employment-icon"></div>
        <h5 class="employment-hdr">Annual Openings</h5>
        <div class="annual-openings">${career['annual-openings']}</div>
      </div>
      <div class="grow-rate-block">
        <div class="employment-icon"></div>
        <h5 class="employment-hdr">Growth Rate</h5>
        ${career['employment'].map(calculateGrowthRate).join('')}
      </div>
    </div>
  `;
}

export { careerJob, calculatePastGrowthRate, calculateFutureGrowthRate }