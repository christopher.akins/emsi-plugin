
const educationChartOptions = function(edArr) {
  console.log(edArr['education-attainment-levels']);

  var data = {
    labels: [
      'High school diploma or less',
      'A certificate',
      'Some college',
      'An Associate degree',
      'A Bachelor\'s degree',
      'A Master\'s degree or Professional degree',
      'A Doctoral degree or more'
    ],

    series: [
      [0, 5, 10, 45, 20, 20, 0]
    ]
    
  }

  return data;
};

export { educationChartOptions }