<?php

require_once( EMSI_API_PLUGIN_DIR . '/classes/class.emsi-connection.php' );

add_action( 'wp_ajax_pull_emsi_data', 'pull_emsi_handler' );
add_action( 'wp_ajax_nopriv_pull_emsi_data', 'pull_emsi_hander' );

function pull_emsi_programs_handler() {
    check_ajax_referer( 'emsi_data', 'nonce_security' );

    $program_name = $_POST['program_name'];
    $region = $_POST['region'];
    $geo_id = $_POST['geo_id'];

    $emsi_data = EMSI_CONNECTION::get_url_endpoint( '/programs/scitexas/cyber-security-program', false ); 
    $emsi_data = json_decode($emsi_data);

    wp_send_json($emsi_data);

    echo $program_name;

    wp_die();
}

/*
 * RETRIEVES THE FOLLOWING FROM THE /careers/ API
 *
 * Humanized title
 * Singular title
 * Annual Earnings
 * Annual Openings
 * Skills (soft skills)
 * Employment (projection)
 * 
 */ 

// Change $data_query_string variable within the handler

add_action( 'wp_ajax_pull_emsi_all_career_data', 'pull_emsi_all_career_handler' );
add_action( 'wp_ajax_nopriv_pull_emsi_all_career_data', 'pull_emsi_all_career_handler' );

function pull_emsi_all_career_handler() {
    check_ajax_referer( 'emsi_data', 'nonce_security' );

    $data_query_string = '?fields=humanized-title%2Csingular-title%2Cannual-earnings%2Cannual-openings%2Cskills%2Cemployment';

    // @TODO:// Create a catch statement if we don't have a career_id
    $career_id = $_POST['career_id'] ? $_POST['career_id'] : null;
    $program_name = $_POST['program_name'] ? $_POST['program_name'] : EMSI_API_PROGRAM_DEFAULT;
    $region = $_POST['region'] ? $_POST['region'] : EMSI_API_REGION_DEFAULT;
    $geo_id = $_POST['geo_id'] ? $_POST['geo_id'] : EMSI_API_GEO_ID_DEFAULT;

    $emsi_data = EMSI_CONNECTION::get_url_endpoint( '/careers/us/' . $region . '/' . $geo_id . '/' . $career_id, $data_query_string ); 
    $emsi_data = json_decode($emsi_data);

    wp_send_json($emsi_data->data->attributes);
    
    wp_die();
}

/*
 * RETRIEVES THE FOLLOWING FROM THE /jobs/  API
 *
 * SKILLS (10)
 */ 

add_action( 'wp_ajax_pull_emsi_job_data', 'pull_emsi_job_skills_handler' );
add_action( 'wp_ajax_nopriv_pull_emsi_job_data', 'pull_emsi_job_skills_handler' );

function pull_emsi_job_skills_handler() {
    check_ajax_referer( 'emsi_data', 'nonce_security' );

    $career_id = $_POST['career_id'] ? $_POST['career_id'] : null;
    $program_name = $_POST['program_name'] ? $_POST['program_name'] : EMSI_API_PROGRAM_DEFAULT;
    $region = $_POST['region'] ? $_POST['region'] : EMSI_API_REGION_DEFAULT;
    $geo_id = $_POST['geo_id'] ? $_POST['geo_id'] : EMSI_API_GEO_ID_DEFAULT;

    $emsi_data = EMSI_CONNECTION::get_url_endpoint( '/jobs/us/' . $region . '/' . $geo_id . '/' . $career_id . '/skills?limit=10', false); 
    $emsi_data = json_decode($emsi_data);

    wp_send_json($emsi_data->data->attributes);
    
    wp_die();
}


add_action( 'wp_ajax_pull_emsi_testing', 'pull_emsi_testing' );
add_action( 'wp_ajax_nopriv_pull_emsi_testing', 'pull_emsi_testing' );

function pull_emsi_testing() {
    check_ajax_referer( 'emsi_data', 'nonce_security' );

    $career_id = $_POST['career_id'];
    $region = 'nation';
    $geo_id = 0;

    $emsi_data = EMSI_CONNECTION::get_url_endpoint( '/careers/us/' . $region . '/' . $geo_id . '/' . $career_id, '?fields=humanized-title%2Cannual-earnings%2Cannual-openings%2Cskills' ); 
    $emsi_data = json_decode($emsi_data);

    wp_send_json($emsi_data->data->attributes);
    
    //echo $emsi_data;
    
    wp_die();
}

// class EMSI_AJAX_CONNECTION {


// }