<?php

/*
 * [SHORTCODE] EMSI_SECTION
 * Notes: Has surrounding markup, but the shortcode, `emsi_table` should be used instead. This is a sample/template.
 * 
 */

function emsi_section_shortcode( $atts ) {

    extract( shortcode_atts( array(
        'program' => 'software-developer-program',
        'region' => 'nation', // default will be nation
        'geo_id' => '0', // USA
        'table_width' => '100%'
    ), $atts, 'emsi_data' ) );

    wp_enqueue_script( 'emsi-ajax' );
    wp_enqueue_style( 'emsi-styles' );

    // Get initial payload from the correct program
    $initial_payload = EMSI_CONNECTION::get_url_endpoint( '/programs/scitexas/' . $program, false ); 
    
    // parse the payload
    $payload = json_decode( $initial_payload );
    $payload_data = $payload->data;

    // drill down to get the career ids as an array, then encode it as JSON
    $careers_arr = $payload_data->attributes->careers;
    $program_name = $payload_data->attributes->name;
    $careers_arr = json_encode($careers_arr);

    ob_start(); ?>

    <!-- Start of EMSI Section -->
    <div class="emsi-section">
        <div class="static-content">
            <h2>Careers for a <?= $program_name ?> Program</h2>
            <p>Find the career that matches your native genius... and your desired income.</p>
        </div>
        <div class="dynamic-content">

            <div class="emsi-table" style="max-width: <?= $table_width; ?>">
                <div class="tr table-hdr-row">
                    <div class="tc tc-head">Occupation</div>
                    <div class="tc tc-head">Median Wage</div>
                    <div class="tc tc-head">Employment</div>
                </div>

                <div class="loading-content">
                    <div>
                        <h4>Loading <?= $program_name ?> Careers</h4>
                        <img src="/wp-content/plugins/emsi-api/assets/img/woz-loader.gif" alt="A loader" />
                    </div>
                </div>

                <div id="emsi_data">
                    <div class="careers-list-table">
                        <div class="tr career-row">
                            <div class="tc tc-body tc-occupation"><span class="plus-minus"></span>Loading Data...</div>
                            <div class="tc tc-body tc-earnings">Loading Data...</div>
                            <div class="tc tc-body tc-openings">Loading Data...</div>
                        </div>
                        <div class="tr career-row">
                            <div class="tc tc-body tc-occupation"><span class="plus-minus"></span>Loading Data...</div>
                            <div class="tc tc-body tc-earnings">Loading Data...</div>
                            <div class="tc tc-body tc-openings">Loading Data...</div>
                        </div>
                    </div>
                </div> <!-- // #emsi_data -->
    
            </div> <!-- // .emsi-table -->
        </div> <!-- // .dynamic-content -->
    </div>

    <script>
        var careersArr = <?= $careers_arr ?>;
        var shortcodeProgram = '<?= $program ?>';
        var shortcodeRegion = '<?= $region ?>';
        var shortcodeGeoId = '<?= $geo_id ?>';
    </script>
    
    <?php

    return ob_get_clean();
}

/*
 * [SHORTCODE] EMSI_TABLE
 * Notes: Only the table and no surrounding markup. 
 * 
 */

function emsi_table_shortcode( $atts ) {
    extract( shortcode_atts( array(
        'program' => 'software-developer-program',
        'region' => 'nation', // default will be nation
        'geo_id' => '0', // USA
        'table_width' => '100%'
    ), $atts, 'emsi_data' ) );

    wp_enqueue_script( 'emsi-ajax' );
    wp_enqueue_script( 'chart-library' );
    wp_enqueue_style( 'emsi-styles' );

    // Get initial payload from the correct program
    $initial_payload = EMSI_CONNECTION::get_url_endpoint( '/programs/scitexas/' . $program, false ); 
    
    // parse the payload
    $payload = json_decode( $initial_payload );
    $payload_data = $payload->data;

    // drill down to get the career ids as an array, then encode it as JSON
    $careers_arr = $payload_data->attributes->careers;
    $program_name = $payload_data->attributes->name;
    $careers_arr = json_encode($careers_arr);

    ob_start(); ?>

    <div class="emsi-table" style="max-width: <?= $table_width; ?>">
        <div class="tr table-hdr-row">
            <div class="tc tc-head">Occupation</div>
            <div class="tc tc-head">
                <span>Avg. Starting Salary</span>
                <div class="data-tooltip">?</div>
                <div class="tooltip-modal">
                    <div class="tooltip-content-wrapper">
                        <p>The Bureau of Labor Statistics does not report growth and retirement data granular enough to pinpoint career specializations. Data from the "parent" classification - Computer Occupations, All Other - has been substituted in some cases. As a result, this pay data does not reflect any special compensation one may receive for these careers.</p>
                    </div>
                </div>
            </div>
            <div class="tc tc-head">
                <span>Annual Openings</span>

                <!-- <div class="data-tooltip">?</div>
                <div class="tooltip-modal">
                    <div class="tooltip-content-wrapper">
                        <p>Job growth projections are based off national labor market employment data. Projected data shows annual average increase/decrease until 2030.</p>
                    </div>
                </div> -->
            </div>
        </div>

        <div id="emsi_data">

            <div class="careers-list-table">
                <div class="tr career-row">
                    <div class="tc tc-body tc-occupation"><span class="plus-minus"></span>Loading Data...</div>
                    <div class="tc tc-body tc-earnings">Loading Data...</div>
                    <div class="tc tc-body tc-openings">Loading Data...</div>
                </div>
                <div class="tr career-row">
                    <div class="tc tc-body tc-occupation"><span class="plus-minus"></span>Loading Data...</div>
                    <div class="tc tc-body tc-earnings">Loading Data...</div>
                    <div class="tc tc-body tc-openings">Loading Data...</div>
                </div>
            </div>
        </div> <!-- // #emsi_data -->

        <div class="loading-content">
            <div>
                <h4>Loading <?= $program_name ?> Careers</h4>
                <img src="/wp-content/plugins/emsi-api/assets/img/woz-loader.gif" alt="A loader" />
            </div>
        </div>
    </div> <!-- // .emsi-table -->

    <script>
        var careersArr = <?= $careers_arr ?>;
        var shortcodeProgram = '<?= $program ?>';
        var shortcodeRegion = '<?= $region ?>';
        var shortcodeGeoId = '<?= $geo_id ?>';
    </script>
    
    <?php

    return ob_get_clean();
}

add_shortcode( 'emsi_section', 'emsi_section_shortcode' );
add_shortcode( 'emsi_table', 'emsi_table_shortcode' );
