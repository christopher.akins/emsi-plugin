<?php

function emsi_api_plugin_scripts() {

    // Register the external script
    wp_register_script( 'emsi-ajax', plugins_url( '/emsi-api/dist/main.js' ), array(), false, true );
    wp_register_script( 'chart-library', plugins_url('/emsi-api/src/js/lib/chartist.js'), array(), null, true );

    // Register the external styles
    wp_register_style( 'emsi-styles', plugins_url( '/emsi-api/dist/style.css' ), array(), false, false );

    // Localize the AJAX data and the nonce
    wp_localize_script( 'emsi-ajax',  'emsi_api_obj',
        array(
            'ajaxurl' => '/wp-admin/admin-ajax.php',
            'nonce_security' => wp_create_nonce( 'emsi_data' ),
            'assets_dir' => plugins_url('/emsi-api/assets')
        )
    );
}

add_action( 'wp_enqueue_scripts', 'emsi_api_plugin_scripts' );